name := """holla"""

version := "1.0-SNAPSHOT"

playJavaSettings

ebeanEnabled := false

libraryDependencies ++= Seq(
    javaCore,
    jdbc,
    javaJpa,
	"commons-io" % "commons-io" % "2.3",
	"org.springframework" % "spring-context" % "3.2.2.RELEASE",
    "javax.inject" % "javax.inject" % "1",
    "org.springframework.data" % "spring-data-jpa" % "1.9.0.RELEASE",
    "org.springframework" % "spring-expression" % "3.2.2.RELEASE",
    "org.hibernate" % "hibernate-entitymanager" % "3.6.10.Final",
    "mysql" % "mysql-connector-java" % "5.1.18",
    "org.mockito" % "mockito-core" % "1.9.5" % "test",
    "org.javassist" % "javassist" % "3.18.2-GA"
)