package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import play.GlobalSettings;
import play.libs.Json;
import play.mvc.Result;
import play.test.WithApplication;
import util.Base64;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

/**
 * An integration test focused on testing our routes configuration and interactions with our controller.
 * However we can mock repository interactions here so we don't need a real db.
 */
@RunWith(MockitoJUnitRunner.class)
public class HollaServiceIT extends WithApplication {

    private HollaService holla;
    private ObjectMapper mapper;

    @Mock    private PersonRepository repo;
    @Mock    private UserRepository userRepository;
    @Mock    private MessageRepository messageRepository;
    @Mock    private VoteRepository voteRepository;
    @Mock    private TaggingRepository taggingRepository;
    @Mock    private AttachmentRepository attachmentRepository;


    @Before
    public void setUp() throws Exception {
    	holla = new HollaService(userRepository, 
        						messageRepository, 
        						voteRepository,
        						taggingRepository,
                                attachmentRepository);

        mapper = new ObjectMapper();
        final GlobalSettings global = new GlobalSettings() {
            @Override
            public <A> A getControllerInstance(Class<A> aClass) {
                return (A) holla;
            }
        };

        start(fakeApplication(global));
    }

    /**
    @Test
    public void indexSavesDataAndReturnsId() {
        final Person person = new Person();
        person.id = SOME_ID;
        when(repo.save(any(Person.class))).thenReturn(person);
        when(repo.findOne(SOME_ID)).thenReturn(person);

        final Result result = route(fakeRequest(GET, "/"));

        assertEquals(OK, status(result));
        assertTrue(contentAsString(result).contains(SOME_CONTENT_MESSAGE));
    }
    */

    @Test
    public void getUser() {
        User user = this.userRepository.findOne(1L);
        user = this.userRepository.findByAuthToken("c82f7836-aa17-469a-aa3a-6f3f1f57f374");
        System.out.println(user);
    }


    @Test
    public void testRegisterUser() {
        String body = "{ " + 
        					"\"phoneNum\":\"919-371-8188\",\n"+
        					"\"handleName\":\"BigCheese\",\n"+
        					"\"location\":{\n"+
							"\"longitude\":\"34.137636\",\n"+
							"\"latitude\":\"84.134089\"\n"+
						"}\n"+
					"}";
        final Result result = route(fakeRequest(POST, "/registerUser").withJsonBody(Json.parse(body)));

        System.out.println(result);
        assertEquals(OK, status(result));
        
        String res = contentAsString(result);
        System.out.println(res);
        User u = Json.fromJson(Json.parse(res), User.class);

        assertTrue(u != null);
        assertTrue(u.id != null);
        /*
        assertTrue(u.id > 0);
        */
    }

    @Test
    public void testPostMobilePicture() {
        try {
            HollaService.PostMobilePictureIn inObj = new HollaService.PostMobilePictureIn();
            inObj.messageID = 1;
            HollaService.EncodedImage image = new HollaService.EncodedImage();
            image.fileName = "222.jpg";
            image.encodedStr = Base64.encodeFromFile(image.fileName);
            inObj.pictures.add(image);

            JsonNode json = mapper.convertValue(inObj, JsonNode.class);
            final Result result = route(fakeRequest(POST, "/postMobilePictures")
                    //.withHeader("X-AUTH-TOKEN", "c82f7836-aa17-469a-aa3a-6f3f1f57f374")
                    .withJsonBody(json));
            System.out.println(result);
            assertEquals(OK, status(result));

            String res = contentAsString(result);
            System.out.println(res);
            HollaService.PostMobilePictureOut outObj =
                    Json.fromJson(Json.parse(res), HollaService.PostMobilePictureOut.class);
            assertTrue(outObj.messageID == 1);
            assertTrue(outObj.failedPictures.size() == 0);
        } catch (IOException ioe) {

        }
    }
}
