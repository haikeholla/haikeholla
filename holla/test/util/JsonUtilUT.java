package util;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import play.libs.Json;

/**
 * Created by jigsaw on 2015/10/22.
 */
public class JsonUtilUT {

    @Test
    public void testGet() {
        final String jsonStr = "{\"location\":{\"longitude\":\"-84.17718898\",\"latitude\":\"33.86440906\"},\"page\":0 ,\"voteDesc\":true}";
        JsonNode json = Json.parse(jsonStr);
        System.out.println(json.get("voteDesc"));
        boolean voteDesc = JsonUtil.get(json, "voteDesc", false, Boolean.class);
        assert(voteDesc == true);
    }
}
