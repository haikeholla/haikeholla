angular.module('hollaApp', []).controller('hollaCtrl', function($scope, $http) {
	$scope.messages = [];
	$scope.messagesCurrentPage = 0;
	$scope.location = null;
	$scope.authCode = null;
	
	$scope.postMessage = {title:'', content:''};

	$scope.selectMessage = null;
	$scope.replies = null;

	$scope.postReply = '';

	$scope.picture = null;
	$scope.picPreviewURL = null;

    $scope.imageSelected = function (evt) {
		if (!evt.target.files[0].type.match('image.*')) {
		    return;
		}
		
        $scope.picture = evt.target.files[0];
        
		var f = $scope.picture;
		
		var reader = new FileReader();
		
		// Closure to capture the file information.
		reader.onload = (function(theFile) {
		    return function(e) {
		      $scope.picPreviewURL = e.target.result;
		      document.getElementById('picPreview').src=$scope.picPreviewURL;
		      if ($scope.picPreviewURL){
		      	$("#picPreviewPanel").removeClass('hidden');
		      }
		    };
		})(f);
		
		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
    };
    
    $scope.removePicture = function () {
    	$scope.picPreviewURL = null;
    	$scope.picture = null;
    	
    	var control = $("#picPreview");
    	control.replaceWith( control = control.clone( true ) );
   	
    	$("#picPreviewPanel").addClass('hidden');
    };
    
	$scope.voteOnPost = function(voteMessage, isUp) {
		$scope.ensureLocation( function() {
			$scope.ensureAuthCode( function() {
		    	var config = {headers:{ "X-AUTH-TOKEN" : $scope.authCode}};
				$http.post('/voteOnPost', {id:voteMessage.id, upVote:isUp}, config).
					success(function(data, status, headers, config) {
					    voteMessage.voteScore = data.voteScore;
					    voteMessage.myVote = data.myVote;
					}).
					error(function(data, status, headers, config) {
					});
			});
		});
	};
	
	$scope.chooseMessage = function(selMessage) {
		$scope.selectMessage = selMessage;
		$scope.getReplies();
	};
	
	$scope.getReplies = function(opid) {
		var opMessageID = null;
		if (opid){
			opMessageID = opid;
		}else{
			opMessageID = $scope.selectMessage.id
		}
	
		$scope.ensureLocation( function() {
			$scope.ensureAuthCode( function() {
		    	var config = {headers:{ "X-AUTH-TOKEN" : $scope.authCode}};
				$http.get('/getReplies/' + opMessageID, config).
					success(function(data, status, headers, config) {
						$scope.selectMessage = data;
					    $scope.replies = data.replies;
					    $scope.selectMessage.replyCount = $scope.replies.length;
					    $scope.gotoDetail();
					    document.title = "吼啦！" + $scope.selectMessage.messageContent;
					}).
					error(function(data, status, headers, config) {
					    $scope.replies = [];
					});
			});
		});
	};
	
	$scope.submitReply = function() {
		$scope.ensureLocation( function() {
			$scope.ensureAuthCode( function() {
		    	var config = {headers:{ "X-AUTH-TOKEN" : $scope.authCode}};
				$http.post('/postReply', 
							{	replyTo:$scope.selectMessage.id, 
								message:$scope.postReply, 
								location:$scope.location
							}, config).
					success(function(data, status, headers, config) {
						$scope.postReply = '';
						$scope.getReplies();
					}).
					error(function(data, status, headers, config) {
						alert('我出问题了！');
					});
			});
		});
	};
	
	$scope.submitPost = function() {
	
		var cleanUpPost = function(){
			$scope.removePicture();
			
			$scope.postMessage.content = '';
			$scope.getLocalMessages();
			$scope.gotoMessages();
		}
		
		$scope.submitPostData(function(messageID){
		
			if ($scope.picture){
		    	var config = {
		    					transformRequest: angular.identity,
		    					headers:{ 
		    						"X-AUTH-TOKEN" : $scope.authCode,
		    						'Content-Type': undefined
		    					}
		    				};
		    	
		        var fd = new FormData();
		        fd.append('messageID', messageID);
		        fd.append('picture', $scope.picture);
		        
		        $http.post('/postPicture', fd, config)
		        .success(function(data){
		        	console.log(data);
					cleanUpPost();
		        })
		        .error(function(){
					alert('我出问题了！');
		        });
			}else{
				cleanUpPost();
			}
		});
	};
	
	$scope.submitPostData = function(callback) {
		$scope.ensureLocation( function() {
			$scope.ensureAuthCode( function() {
		    	var config = {
		    					headers:{ 
		    						"X-AUTH-TOKEN" : $scope.authCode
		    					}
		    				};

				$http.post('/postMessage', 
							{	
								replyTo:-1, 
								message:$scope.postMessage.content, 
								location:$scope.location
							}, 
							config).
					success(function(data, status, headers, config) {
						callback(data.id);
					}).
					error(function(data, status, headers, config) {
						alert('我出问题了！');
					});
			});
		});
	};
	
	$scope.cancelPost = function() {
		$scope.gotoMessages();
	};

	$scope.gotoMessages = function() {
		$("#btnPost").removeClass('hidden');
		$("#btnRefresh").removeClass('hidden');
		$("#btnMsgs").addClass('hidden');

		$("#messagesPanel").removeClass('hidden');
		$("#postPanel").addClass('hidden');
		$("#detailPanel").addClass('hidden');
		
		document.title = "本日更新";
	};

	$scope.gotoPost = function() {
		$("#btnPost").addClass('hidden');
		$("#btnRefresh").addClass('hidden');
		$("#btnMsgs").removeClass('hidden');

		$("#messagesPanel").addClass('hidden');
		$("#postPanel").removeClass('hidden');
		$("#detailPanel").addClass('hidden');
	};

	$scope.gotoDetail = function() {
		$("#btnPost").addClass('hidden');
		$("#btnRefresh").addClass('hidden');
		$("#btnMsgs").removeClass('hidden');

		$("#messagesPanel").addClass('hidden');
		$("#postPanel").addClass('hidden');
		$("#detailPanel").removeClass('hidden');
	};

	$scope.imageLocRes = function(imgUrl){
	    return window.location.protocol + "//" + window.location.hostname + ':9000/assets/images/' + imgUrl ;
	    // return 'images/' + imgUrl ;
    };
	
	$scope.friendlyTime = function(tPast){
    	var timeInMs = Date.now();
    	var diffInMs = timeInMs - tPast;

    	var diffInDays = Math.floor(diffInMs / (3600 * 1000 * 24));
    	if (diffInDays >= 1){
    		return  '' + diffInDays + ' 天前';
    	}else{
	    	var diffInHours = Math.floor(diffInMs / (3600 * 1000));
	    	if (diffInHours >= 1){
	    		return  '' + diffInHours + ' 小时前';
	    	}else{
		    	var diffInMins = Math.floor(diffInMs / (60 * 1000));
		    	if (diffInMins >= 1){
		    		return  '' + diffInMins + ' 分钟前';
	    		}
	    	}
	    }	
	    return '刚才';
    };
	
	$scope.getLocalMessages = function(nextPage) {
	
		var pageN = 0;
		if (nextPage){
			pageN = $scope.messagesCurrentPage + 1;
		}
	
		$scope.ensureLocation( function() {
			$scope.ensureAuthCode( function() {
		    	var config = {headers:{ "X-AUTH-TOKEN" : $scope.authCode}};
				$http.post('/getLocalMessages', {
													location:$scope.location,
													page:pageN 
												}, 
												config).
					success(function(data, status, headers, config) {
						console.log(data);
						if (nextPage){
						    $scope.messages.push.apply($scope.messages, data);
						}else{
						    $scope.messages = data;
						}
					    $scope.messagesCurrentPage = pageN;
					}).
					error(function(data, status, headers, config) {
					});
			});
		});
	};
	
	$scope.loadMoreMessages = function(){
		$scope.getLocalMessages(true);
	}
	
	$scope.ensureLocation = function(callback){
		if (! $scope.location ){
		    if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition( function(position){
			    	$scope.location = {
			    		longitude: position.coords.longitude,
			    		latitude: position.coords.latitude
			    	};
			    	callback();
		        } );
		        return;
		    }else{
	        	$scope.location = {
	        		longitude: 0,
	        		latitude: 0
	        	}
		    } 
		}
		callback();
	}
	
	$scope.ensureAuthCode = function(callback) {
		if (! $scope.authCode ){
			var ac = localStorage.getItem('HollaAuthCode');
			if (ac){
				$scope.authCode = ac;
			}else{
				$scope.ensureLocation(function(){
					$http.post('/registerUser', {phoneNum:'', handleName:'', location:$scope.location}).
						success(function(data, status, headers, config) {
						    $scope.authCode = data.authToken;
						    localStorage.setItem('HollaAuthCode', data.authToken);
						    callback();
						});
					return;
				});
			}
		}
		callback();
	};

	$scope.getHotMessages = function(page) {
		$http.post('/validateLogin',
		{
        	username: $scope.username,
        	password: $scope.password
        }).
        success(function(data, status, headers, config) {

        }).
        error(function(data, status, headers, config) {
        	alert('invalid username and password');
        });
	};
	
	$scope.initialize = function(){
	
		var view = window.location.pathname;
		
		if (view.indexOf("detail") > -1){
			var opid = $scope.getParameterByName("opid");
			$scope.getReplies(opid);
		}else{
			$scope.getLocalMessages();
			$scope.gotoMessages();
		}
	}

	$scope.getParameterByName = function(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
})