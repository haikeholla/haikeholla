angular.module('hollaAdminApp', []).controller('hollaAdminCtrl', function($scope, $http) {
    $scope.registerInfo = {phone:'', handle:''};
    $scope.authCode = '';
    $scope.userRole = 'user';
    $scope.registerUser = function() {
        $http.post('/admin/register',
            {
                phoneNum:$scope.registerInfo.phone,
                handleName:$scope.registerInfo.handle,
                location:{ "longitude":"-999",
         					"latitude":"-999"
                }
            }).
        success(function(data, status, headers, config) {
            alert(data.authToken);
            $scope.authCode = data.authToken;
        }).
        error(function(data, status, headers, config) {
            alert('there is something wrong: ' + data);
        });
    };

    $scope.cancel = function() {
        $scope.registerInfo.phone = '';
        $scope.registerInfo.handle = '';
    };

    $scope.activateUser = function() {
        $http.post('/admin/activate',
        {
             authToken:$scope.authCode,
             handle:$scope.registerInfo.handle,
             userRole:$scope.userRole
        }).
        success(function(data, status, headers, config) {
            alert(data.authToken);
        }).
        error(function(data, status, headers, config) {
             alert('there is something wrong: ' + data);
        });
    };
});
