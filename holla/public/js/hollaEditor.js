angular.module('hollaEditorApp', []).controller('hollaEditorCtrl', function($scope, $http) {
	$scope.messages = [];
	$scope.messagesCurrentPage = 0;
	$scope.location = {latitude:34.0329419, longitude:-84.19624189999999};
	$scope.authCode = null;

	$scope.postMessage = {title:'', content:''};

	$scope.selectMessage = null;
	$scope.replies = null;

	$scope.postReply = '';

	$scope.picture = null;
	$scope.picPreviewURL = null;

    $scope.imageSelected = function (evt) {
    	if (!evt.target.files[0].type.match('image.*')) {
    		return;
    	}

        $scope.picture = evt.target.files[0];

    	var f = $scope.picture;

    	var reader = new FileReader();

    	// Closure to capture the file information.
    	reader.onload = (function(theFile) {
    		return function(e) {
    		    $scope.picPreviewURL = e.target.result;
    		    document.getElementById('picPreview').src=$scope.picPreviewURL;
    		    if ($scope.picPreviewURL){
    		      	$("#picPreviewPanel").removeClass('hidden');
    		    }
    		};
    	})(f);

    	// Read in the image file as a data URL.
    	reader.readAsDataURL(f);
    };

    $scope.removePicture = function () {
        	$scope.picPreviewURL = null;
        	$scope.picture = null;

        	var control = $("#picPreview");
        	control.replaceWith( control = control.clone( true ) );

        	$("#picPreviewPanel").addClass('hidden');
        };

        	$scope.submitPost = function() {

        		var cleanUpPost = function(){
        			$scope.removePicture();

        			$scope.postMessage.content = '';
        			$scope.postMessage.title = '';
        			$scope.getLocalMessages();
        		}

        		$scope.submitPostData(function(messageID){

        			if ($scope.picture){
        		    	var config = {
        		    					transformRequest: angular.identity,
        		    					headers:{
        		    						"X-AUTH-TOKEN" : $scope.authCode,
        		    						'Content-Type': undefined
        		    					}
        		    				};

        		        var fd = new FormData();
        		        fd.append('messageID', messageID);
        		        fd.append('picture', $scope.picture);

        		        $http.post('/postPicture', fd, config)
        		        .success(function(data){
        		        	console.log(data);
        					cleanUpPost();
        		        })
        		        .error(function(){
        					alert('??????');
        		        });
        			}else{
        				cleanUpPost();
        			}
        		});
        	};

    $scope.submitPostData = function(callback) {
        $scope.ensureLocation( function() {
        	$scope.ensureAuthCode( function() {
        		var config = {
        		    headers:{
        		    	"X-AUTH-TOKEN" : $scope.authCode
        		    }
        		};

				$http.post('/postMessage',
				{
					replyTo: -1,
					title: $scope.postMessage.title,
					message: $scope.postMessage.content,
					location: $scope.location
				},
				config).
				success(function(data, status, headers, config) {
					callback(data.id);
				}).
				error(function(data, status, headers, config) {
					alert('??????');
				});
        	});
        });
    };

    $scope.cancelPost = function() {
        $scope.gotoMessages();
    };

    $scope.imageLocRes = function(imgUrl){
        return window.location.protocol + "//" + window.location.hostname + ':9000/assets/images/' + imgUrl ;
        // return 'images/' + imgUrl ;
    };

	$scope.loadMoreMessages = function(){
		$scope.getLocalMessages(true);
	}

	$scope.ensureAuthCode = function(callback) {
		if (! $scope.authCode ){
			var ac = localStorage.getItem('HollaAuthCode');
			if (ac){
				$scope.authCode = ac;
			}else{
				$scope.ensureLocation(function(){
					$http.post('/registerUser', {phoneNum:'', handleName:'', location:$scope.location}).
						success(function(data, status, headers, config) {
						    $scope.authCode = data.authToken;
						    localStorage.setItem('HollaAuthCode', data.authToken);
						    callback();
						});
					return;
				});
			}
		}
		callback();
	};


	$scope.ensureLocation = function(callback){
		if (! $scope.location ){
		    if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition( function(position){
			    	$scope.location = {
			    		longitude: position.coords.longitude,
			    		latitude: position.coords.latitude
			    	};
			    	callback();
		        } );
		        return;
		    }else{
	        	$scope.location = {
	        		longitude: 0,
	        		latitude: 0
	        	}
		    }
		}
		callback();
	}

	$scope.getLocalMessages = function(nextPage) {

		var pageN = 0;
		if (nextPage){
			pageN = $scope.messagesCurrentPage + 1;
		}

		var config = {headers:{ "X-AUTH-TOKEN" : $scope.authCode}};
        $http.post('/editor/getMyPosts', {
        	location:$scope.location,
        	page:pageN
        },
        config).
        success(function(data, status, headers, config) {
        	if (nextPage){
        		$scope.messages.push.apply($scope.messages, data);
        	}else{
        		$scope.messages = data;
        	}
        		$scope.messagesCurrentPage = pageN;
        	}).
        	error(function(data, status, headers, config) {
        });
	};

	$scope.initialize = function(){

		$scope.getLocalMessages();
	}

	$scope.getParameterByName = function(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
});