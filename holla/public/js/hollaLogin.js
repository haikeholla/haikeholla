angular.module('hollaLogin', []).controller('hollaLoginCtrl', function($scope, $http) {
	$scope.loginInfo = {
		username:'',
		password:''
	};

	$scope.login = function() {
		$http.post('/validateLogin',
		{
        	username: $scope.loginInfo.username,
        	password: $scope.loginInfo.password
        }).
        success(function(data, status, headers, config) {
			window.location.href = data.nextPage;
        }).
        error(function(data, status, headers, config) {
        	alert('invalid username and password');
        });
	}
});