package filters;

import models.Message;
import models.User;

import java.util.List;

/**
 * Created by jigsaw on 2015/10/27.
 */
public interface Targeting {
    public List<Message> generateTargetedTopicsForUser(User user, List<Message> rawMessages);
}
