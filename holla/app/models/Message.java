package models;

import controllers.request.PostMessageIn;

import javax.persistence.*;
import java.util.*;

@Entity
public class Message {
	
	/**
	 * System generated id 
	 * */
    @Id
    @GeneratedValue
    public Long id;

    public String   title;

    public String 	messageContent;
    
    @ManyToOne(fetch = FetchType.EAGER)
    public User author;
    
    public boolean showAuthor = false;
    
    @ManyToOne(fetch = FetchType.EAGER)
    public Message inReplyTo;

	@OneToMany(mappedBy="inReplyTo", fetch = FetchType.LAZY)
    public List<Message> replies;
	
	/**
	 * Replies should not be loaded eagerly. 
	 * Also, replies should not be loaded just to get a count.  
	 * 
	 * */
	public int replyCount=0;
    
	@OneToMany(mappedBy="message", fetch = FetchType.EAGER)
    public Set<Vote> votes;
	
	public int voteScore = 0;
    
    @OneToMany(mappedBy="message", fetch = FetchType.EAGER)
    public List<Tagging>	tagging;

    @OneToMany(mappedBy="message", fetch = FetchType.EAGER)
    public Set<Attachment>	attachment;

    @Embedded
    public Location location;
    
    public Date postTime;

    public boolean isRegularMsg() {
        return author.isRegularUser();
    }
    
    public Message(){
        postTime = new Date();
        replies = new LinkedList<Message>();
        votes   = new HashSet<Vote>();
        tagging = new LinkedList<Tagging>();
        attachment = new HashSet<Attachment>();
    }
    
    public Message(User u, String text){
        this();
    	author = u;
    	messageContent = text;
    }
    
    public Message(User u, String text, Message replyTo, Location loc){
        this(u, text);
    	inReplyTo = replyTo;
    	location = loc;
    }

    public Message(User u, PostMessageIn postMes) {
        this(u, postMes.message);
        this.title = postMes.title;
        this.location = postMes.location;
    }
    
    /** 
     * TODO: Think about Image/Video/Audio Attachment Later. 
    public bytes[] image;
    */
    
    @Override
    public boolean equals(Object bMessage){
    	return (bMessage instanceof Message) && this.id == ((Message)bMessage).id;
    }
}
