package models;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import controllers.response.AttachmentJson;
import controllers.response.ImageAttachmentJson;

import javax.persistence.*;
import java.io.IOException;

@Entity
public class Attachment {
	
	public static final String IMAGE_TYPE = "image";
	public static final String AUDIO_TYPE = "audio";
	public static final String VIDEO_TYPE = "video";
	/**
	 * System generated id 
	 * */
    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    public Message message;

    public String path;  //path relative to public

    public String type;
//    @JsonDeserialize(using = AttachmentTypeDeserializer.class)
//    public void setType(final Type type) {
//        this.type = type;
//    }
//    @JsonSerialize(using = AttachmentTypeSerializer.class)
//    public Type getType() {
//        return type;
//    }
//    public Type type;

    public int imageWidth;
    public int imageHeight;
    public int thumbnailWidth;
    public int thumbnailHeight;
    @Override
    public boolean equals(Object bMessage){
    	return (bMessage instanceof Attachment) && this.id == ((Attachment)bMessage).id;
    }

    public enum Type {
        image,
        audio,
        video,
        unknown;
        public static Type fromTypeCode(final int typeCode) {
            switch(typeCode) {
                case 0: return image;
                case 1: return audio;
                case 2: return video;
                default: return unknown;
            }
        }

        public static Type fromTypeStr(final String typeStr) {
            if(IMAGE_TYPE.equalsIgnoreCase(typeStr)) {
                return image;
            } else if(AUDIO_TYPE.equalsIgnoreCase(typeStr)) {
                return audio;
            } else if(VIDEO_TYPE.equalsIgnoreCase(typeStr)) {
                return video;
            } else {
                return unknown;
            }
        }
    }

    class AttachmentTypeDeserializer extends JsonDeserializer<Type> {
        @Override
        public Type deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
            String val = parser.getValueAsString();
            try {
                int code = Integer.parseInt(val);
                return Type.fromTypeCode(code);
            } catch(NumberFormatException nfe) {
                return Type.fromTypeStr(val);
            }
        }
    }

    class AttachmentTypeSerializer extends JsonSerializer<Type> {
        @Override
        public void serialize(Type val, JsonGenerator generator, SerializerProvider provider) throws IOException,
                JsonProcessingException {
            generator.writeString(val.name());
        }
    }

    public AttachmentJson toJsonClass() {
        switch(type) {
            case "image":
            case "0":
            //case image:
                return new ImageAttachmentJson(Type.image.name(), path, imageWidth, imageHeight, thumbnailWidth, thumbnailHeight);
        }
        return new AttachmentJson(Type.unknown.name(), path);
    }
}
