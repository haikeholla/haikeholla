package models;

import javax.persistence.Embeddable;

@Embeddable
public class Location {
    public double longitude = -99999.9;
    public double latitude = -99999.9;
    // public double altitude;

    /** 
     * TODO: -- Think about address later. 
    public String street;
    public String street2;
    public String xiaoqu;
    public String district;
    public String city;
    public String provState;
    public String country;
    public String zipCode;
    */
}
