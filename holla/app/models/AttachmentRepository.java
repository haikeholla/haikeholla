package models;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Provides CRUD functionality for accessing people. Spring Data auto-magically takes care of many standard
 * operations here.
 */
@Named
@Singleton
public interface AttachmentRepository extends PagingAndSortingRepository<Attachment, Long> {
    //public List<Attachment> findByMessage(Message m);
    @Transactional
    public void deleteByMessage(Message m);
    @Transactional
    public void deleteByMessageId(Long id);
}