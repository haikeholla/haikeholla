package models;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Provides CRUD functionality for accessing people. Spring Data auto-magically takes care of many standard
 * operations here.
 */
@Named
@Singleton
@Transactional
public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {
	public List<Message> findByAuthor(User author);

	@Query("SELECT m FROM Message m LEFT JOIN FETCH m.replies r WHERE m.id = (:id)")
    public Message findByIdAndFetchRepliesEagerly(@Param("id") Long id);

	public List<Message> findByInReplyToOrderByPostTimeDesc(Message op, Pageable pageable);

	public Message findById(Long id);

	public Message findByIdAndAuthorId(Long id, Long mid);

	public List<Message> findByAuthorAndInReplyToNotNullOrderByPostTimeDesc(User author);

	public List<Message> findByAuthorAndInReplyToNullOrderByPostTimeDesc(User author, Pageable pageable);

	public List<Message> findByAuthorRole(User.Role role, Pageable pageable);
}