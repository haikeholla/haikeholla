package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Vote {
	
	/**
	 * System generated id, used as a cookie value. 
	 * */
    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    public Message	message;

    @ManyToOne(fetch = FetchType.EAGER)
    public User 	user;

    public boolean isUp = true;
    
    public Vote(){}

    public Vote(Message m, User u, boolean up){
    	this.message = m;
    	this.user = u;
    	isUp = up;
    }
    
    @Override
    public boolean equals(Object bVote){
    	return (bVote instanceof Vote) && (this.message == ((Vote)bVote).message && this.user == ((Vote)bVote).user);
    }
}
