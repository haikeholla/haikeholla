package models;

import play.Logger;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Entity
public class User {
	public enum Role {
        REGULAR, EDITOR, ADMIN;
//        REGULAR(0), EDITOR(1), ADMIN(2);
//        private final int value;
//
//        Role(int value) {
//            this.value = value;
//        }
//
        public static Role fromValue(int value) {
            for(Role role : values()) {
                if(role.ordinal() == value) {
                    return role;
                }
            }
            return getDefault();
        }
        public static Role fromValue(String value) {
            for(Role role : values()) {
                if(role.name().equalsIgnoreCase(value)) {
                    return role;
                }
            }
            return getDefault();
        }

//
//        public int toValue() {
//            return value;
//        }
//
        public static Role getDefault() {
            return REGULAR;
        }
    }

    /**
	 * System generated id, used as a cookie value. 
	 * 
	 * NOTE: ID should only be sent to the owner of the user, never to any other user.
	 * */
    @Id
    @GeneratedValue
    public Long id;
    
    public Date createDate;
    public Date lastAccessDate;
    
    public String authToken;
    public Date tokenDate;

    public Role role;

//    @Transient
//    public Role getRole() {
//        return Role.fromValue(role);
//    }
//
//    public void setRole(Role role) {
//        this.role = role.toValue();
//    }

    /**
     * Optional attributes
     * */
    public String	phoneNum;
    public String 	handleName;


    /**
     * Current Location    
     * */
    @Embedded
    public Location location;
    
	@OneToMany(mappedBy="author")
    public List<Message> myMessages;
    
	@OneToMany(mappedBy="user")
    public List<Vote> myVotes;
    
    /**
     * Home Location: Use can set a home location.    
     * */
    @Embedded
    @AttributeOverrides({
		@AttributeOverride(name = "longitude", column = @Column(name = "home_longitude")),
		@AttributeOverride(name = "latitude", column = @Column(name = "home_latitude")),
		@AttributeOverride(name = "altitude", column = @Column(name = "home_altitude"))
    })
    public Location homeLocation;
    

    /** 
     * TODO: Think about authentication later. 
    public String userName;
    public String passHash;
    
    public String wechatID;
    public String weiboID;
    public String qqID;
    public String facebookID;
    public String twitterID;
    public String email;
    public String phone;

    public byte[] profilePhoto;
    public boolean showIdentity = false;

    public String pubKey;
    public String authToken;
    public String renewToken;
    */

    public static User registerUser(String phone, String handle) {
        return registerUser(phone, handle, new Location());
    }
    
    public static User registerUser(String phone, String handle, Location loc){
    	
    	User u = new User();
    	u.phoneNum = phone;
    	u.handleName = handle;
    	u.location = loc;
    	u.createDate = new Date(System.currentTimeMillis());
    	u.createToken();
    	u.role = Role.REGULAR;
    	return u;
    }
    
	private String createToken() {
		
		Logger.debug("ATTEMPT: Create token for username: " + this.id);

		this.authToken = generateToken();
		this.tokenDate = new Date(System.currentTimeMillis());;
		
		Logger.debug("Token returned for username: " + this.id);
		return authToken;
	}
	
    private String generateToken(){
		String token = UUID.randomUUID().toString();

		Logger.debug("SUCCESS: Generated Token: " + token);
    	return token; 
    }

    public boolean isRegularUser() {
        return role == Role.REGULAR;
    }

    @Override
    public boolean equals(Object bUser){
    	return (bUser instanceof User) && this.id == ((User)bUser).id;
    }
}
