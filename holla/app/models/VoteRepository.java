package models;

import org.springframework.data.repository.CrudRepository;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Provides CRUD functionality for accessing people. Spring Data auto-magically takes care of many standard
 * operations here.
 */
@Named
@Singleton
public interface VoteRepository extends CrudRepository<Vote, Long> {
	public List<Vote> findByUser(User u);
	public List<Vote> findByUserAndMessage(User u, Message m);
}