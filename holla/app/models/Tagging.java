package models;

import com.google.common.base.Strings;
import play.Logger;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
public class Tagging {
	
	/**
	 * System generated id, used as a cookie value. 
	 * */
    @Id
    @GeneratedValue
    public Long id;


    public String	tag;

    @ManyToOne(fetch = FetchType.EAGER)
    public Message	message;

    public Tagging(){}

    public Tagging(String t, Message m){
    	tag = t;
    	message = m;
    }
    
    @Override
    public boolean equals(Object bTagging){
    	return (bTagging instanceof Tagging) && (this.message == ((Tagging)bTagging).message && this.tag == ((Tagging)bTagging).tag);
    }

    public static class TaggingParser {
        public static Pattern MY_PATTERN = Pattern.compile("#(\\w+|\\W+)");

        public static void fillMsgTag(Message m) {
            if(null == m || Strings.isNullOrEmpty(m.messageContent)) return;
            Matcher mat = MY_PATTERN.matcher(m.messageContent);
            while (mat.find()) {
                String tag = mat.group(1);
                Tagging t = new Tagging(tag, m);
                m.tagging.add(t);
                Logger.debug("Add Msg Tag: " + tag);
            }
        }

        public static void fillLocTag(Message m) {
            String geoTag = getGeographyTag(m.location);
            Tagging t = new Tagging(geoTag, m);
            m.tagging.add(t);
            Logger.debug("Add geo Tag: " + geoTag);
        }

        public static String getGeographyTag(Location loc){
            return "/Space/Earth";
        }

        public static void fillTag(Message m) {
            fillMsgTag(m);
            fillLocTag(m);
        }
    }
}
