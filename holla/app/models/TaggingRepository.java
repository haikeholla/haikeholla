package models;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Provides CRUD functionality for accessing people. Spring Data auto-magically takes care of many standard
 * operations here.
 */
@Named
@Singleton
public interface TaggingRepository extends PagingAndSortingRepository<Tagging, Long> {
	public List<Tagging> findByTagOrderByMessagePostTimeDesc(String tag, Pageable pageable);
	public List<Tagging> findByTagOrderByMessageVoteScoreDescMessagePostTimeDesc(String tag, Pageable pageable);
	@Transactional
	public Long deleteByMessage(Message m);
	@Transactional
	public Long deleteByMessageId(Long mid);
}