package util;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class Thumbnail {
	public static void main(String[] args) throws IOException {
		Thumbnails.of("image.jpg")
		.scale(0.25)
        //.size(160, 160)
        .toFile("thumbnail.jpg");
//		SimpleImageInfo imageInfo = new SimpleImageInfo(new File("original.jpg"));
//		System.out.println(imageInfo);
	}

	public static void generateThumbnail(
			File imageFile,
			int heightThreshold,
			int widthThreshold,
			double scaleFactor,
			String thumbnailPath) throws IOException {

		SimpleImageInfo imageInfo = new SimpleImageInfo(imageFile);
		if (imageInfo.getHeight() > heightThreshold || imageInfo.getWidth() > widthThreshold) {
			Thumbnails.of(imageFile).scale(scaleFactor).toFile(thumbnailPath);
		} else {
			FileUtils.copyFile(imageFile, new File(thumbnailPath));
		}
	}
	
	public static void generateThumbnail(
			String imagePath, 
			int heightThreshold, 
			int widthThreshold,
			double scaleFactor,
			String thumbnailPath) throws IOException {
		File imageFile = new File(imagePath);
		generateThumbnail(imageFile, heightThreshold, widthThreshold, scaleFactor, thumbnailPath
		);
	}

	public static SimpleImageInfo generateThumbnailWithFixedWidth(
			File imageFile,
			int fixedWidth,
			String thumbnailPath) throws IOException {
		SimpleImageInfo imageInfo = new SimpleImageInfo(imageFile);
		int imageWidth = imageInfo.getWidth();
		//ImageUtil.ImageDimension d = new ImageUtil.ImageDimension();
		if(imageInfo.getWidth() > fixedWidth) {
			double ratio = fixedWidth * 1.0 / imageWidth;
			Thumbnails.of(imageFile).scale(ratio).toFile(thumbnailPath);
			//d.setSize(fixedWidth, imageInfo.getHeight() * ratio);
			//d.h = (int) (imageInfo.getHeight() * ratio);
		} else {
			FileUtils.copyFile(imageFile, new File(thumbnailPath));
			//d.setSize(imageInfo.getWidth(), imageInfo.getHeight());
			//d.h = imageInfo.getHeight();
		}
		//d.w = fixedWidth;
		return imageInfo;
	}
}
