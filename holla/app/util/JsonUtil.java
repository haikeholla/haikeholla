package util;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;


/**
 * Created by jigsaw on 2015/10/21.
 */
public class JsonUtil {
    public static <T> T get(JsonNode json, String field, T defaultVal, Class<T> clazz) {
        JsonNode node = json.get(field);
        T ret = defaultVal;
        try {
            ret = Json.fromJson(node, clazz);
        } finally {
            return ret;
        }
    }
}
