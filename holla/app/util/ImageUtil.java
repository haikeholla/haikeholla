package util;

import com.google.common.base.Strings;
import setting.Setting;
import java.io.File;
import java.io.IOException;

/**
 * Created by jigsaw on 2015/10/9.
 */
public class ImageUtil {

    public static class ImageDimension {
        public int w;
        public int h;
        public ImageDimension(int w, int h) {
            this.w = w;
            this.h = h;
        }
        public ImageDimension(){}
    }

    public static class ImageInfo {
        public String url;
        public SimpleImageInfo d;

        public ImageInfo(){}
        public ImageInfo(String url, SimpleImageInfo d) {
            this.url = url;
            this.d = d;
        }
    }

    // due to the play 2.2 bug(cann't try-catch exception(not always) in the controller),
    // I have to put those two methods this way
    public static boolean produceThumbnail(
            String imagePath,
            int heightThreshold,
            int widthThreshold,
            double scaleFactor,
            String thumbnailPath) {
        /*
        boolean ret = false;
        try {
            Thumbnail.generateThumbnail(imagePath, heightThreshold, widthThreshold, scaleFactor, thumbnailPath);
            ret = true;
        } catch(IOException ioe) {
        } finally {
            return ret;
        }
        */
        return produceThumbnail(new File(imagePath), heightThreshold, widthThreshold, scaleFactor, thumbnailPath);
    }

    public static boolean produceThumbnail(
            File imageFile,
            int heightThreshold,
            int widthThreshold,
            double scaleFactor,
            String thumbnailPath) {
        boolean ret = false;
        try {
            Thumbnail.generateThumbnail(imageFile, heightThreshold, widthThreshold, scaleFactor, thumbnailPath);
            ret = true;
        } catch(IOException ioe) {
        } finally {
            return ret;
        }
    }

    public static SimpleImageInfo produceThumbnailWithFixedWidth(
            File imageFile,
            int fixedWidth,
            String thumbnailPath) {
        SimpleImageInfo d = null;
        try {
            d = Thumbnail.generateThumbnailWithFixedWidth(imageFile, fixedWidth, thumbnailPath);
        } catch(IOException ioe) {
            d = new SimpleImageInfo(0,0);
        } finally {
            return d;
        }
    }

    public static boolean saveBase64ImageStrAsFile(String encodedStr, String toFilePath) {
        if(Strings.isNullOrEmpty(encodedStr) || Strings.isNullOrEmpty(toFilePath)) return false;
        boolean ret = false;
        try {
            Base64.decodeToFile(encodedStr, toFilePath);
            ret = true;
        } catch (IOException ioe) {

        } finally {
            return ret;
        }
    }

    public static SimpleImageInfo getThumbnailImageInfo(SimpleImageInfo imageInfo) {
        if(null == imageInfo) {
            return new SimpleImageInfo(0, 0);
        }
        double ratio = Setting.THUMBNAIL_WIDTH * 1.0 / imageInfo.getWidth();
        return new SimpleImageInfo(Setting.THUMBNAIL_WIDTH, (int)(ratio * imageInfo.getHeight()));
    }
}
