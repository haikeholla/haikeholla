package setting;

/**
 * Created by jigsaw on 2015/10/9.
 */
public class Setting {
    public static final String ATTACHMENT_IMAGE_STORE_PREFIX = "public/images";
    public static final String ASSETS_PREFIX = "assets";

    public final static String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String SESSION_USER_AUTH_TOKEN = "authToken";
    public static final String SESSION_USER = "session_user";

    public final static String THUMBNAIL_PREFIX = "thumbnail.";
    public final static int THUMBNAIL_HEIGHT = 100;
    public final static int THUMBNAIL_WIDTH = 320;
    public final static double THUMBNAIL_FACTOR = 0.25;

    public final static int MESSAGE_PER_PAGE = 20;
    public final static int REPLY_PER_PAGE = 20;

    public final static int DELETE_MSG_VOTESCORE_THRESHOLD = -5;
}
