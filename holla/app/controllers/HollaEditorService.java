package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.request.LocalMessage;
import models.*;
import org.springframework.data.domain.PageRequest;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import setting.Setting;
import util.JsonUtil;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jigsaw on 2015/10/28.
 */
@Named
@Singleton
public class HollaEditorService extends Controller {
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final TaggingRepository taggingRepository;
    private final AttachmentRepository attachmentRepository;

    @Inject
    public HollaEditorService(final UserRepository userRepository,
                        final MessageRepository messageRepository,
                        final TaggingRepository taggingRepository,
                        final AttachmentRepository attachmentRepository ) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.taggingRepository = taggingRepository;
        this.attachmentRepository = attachmentRepository;
    }

    public Result mainPage() {
        //Logger.debug(session(Setting.SESSION_USER));
        return getUser(false) == null ? badRequest() : ok(views.html.editor_main.render());
    }

    private User getUser(boolean doHeaderAuth) {

        /**
         * First, check if there is a user in the current session.  This depends on the cookie.
         * This is meant for the web app
         * */
        User user = (User) Http.Context.current().args.get(Setting.SESSION_USER);
        /**
         * If there is no user in the session, get the Auth Token Header.  This is meant for the API client.
         * */
        if (user == null) {
            Logger.debug("No User found in current http context. ");
            String authToken = session(Setting.SESSION_USER_AUTH_TOKEN);
            if(authToken == null) {
                return user;
            }
            user = userRepository.findByAuthToken(authToken);
            if (user != null) {
                Http.Context.current().args.put(Setting.SESSION_USER, user);
                Logger.debug("Found user from current session: " + user.id);
                return user;
            }


        } else {
            Logger.debug("Found user in current http context: " + user.id);
        }
        if(doHeaderAuth) {
            String[] authTokenHeaderValues = Http.Context.current().request().headers().get(Setting.AUTH_TOKEN_HEADER);
            if (!((authTokenHeaderValues != null) &&
                    (authTokenHeaderValues.length == 1) &&
                    (authTokenHeaderValues[0] != null) &&
                    user.authToken.equals(authTokenHeaderValues[0]))) {
                user = null;
            }
        }
        return user;
    }

    public Result getMyPostsSvc() {
        User u = this.getUser(true);
        if(null == u) {
            return badRequest("invalid user!");
        }
        JsonNode json = request().body().asJson();

        Logger.debug("Get Local Messages for: " + json);

        Location location =  Json.fromJson(json.get("location"), Location.class);
        int page = JsonUtil.get(json, "page", 0, Integer.class);
        boolean voteDesc = JsonUtil.get(json, "voteDesc", false, Boolean.class);
        List<Message> myPosts =
                this.messageRepository.findByAuthorAndInReplyToNullOrderByPostTimeDesc(u, new PageRequest(page, Setting.MESSAGE_PER_PAGE));

        List<LocalMessage> allRet = new ArrayList<LocalMessage>();

        for (Message mes : myPosts){

            LocalMessage lm = LocalMessage.copyFromMessage(mes, u);

            allRet.add(lm);
        }

        return ok(Json.toJson(allRet));
    }

}
