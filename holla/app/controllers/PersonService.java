package controllers;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import models.Person;
import models.PersonRepository;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * The main set of web services.
 */
@Named
@Singleton
public class PersonService extends Controller {

	private final PersonRepository personRepository;

	// We are using constructor injection to receive a repository to support our
	// desire for immutability.
	@Inject
	public PersonService(final PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	public Result sayHello() {
        return ok(Json.toJson("Hello"));
	}

	public Result get(Long id) {
        final Person retrievedPerson = personRepository.findOne(id);
        return ok(Json.toJson(retrievedPerson));
	}

	public Result list() {
        final Iterable<Person> allPeople = personRepository.findAll();
        return ok(Json.toJson(allPeople));
	}
}
