package controllers.request;

import models.Location;

/**
 * Created by jigsaw on 2015/11/8.
 */
public class PostMessageIn {
    public long replyTo;
    public String title;
    public String message;
    public Location location;
}
