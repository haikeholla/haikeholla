package controllers.request;

import controllers.response.AttachmentJson;
import models.Attachment;
import models.Message;
import models.User;
import models.Vote;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jigsaw on 2015/11/2.
 */
public class LocalMessage {
    public long id;
    public long replyTo = -1;
    public String title;
    public String messageContent;
    public Date postTime;
    public int voteScore;
    public boolean myMessage = false;
    public int myVote = 0;
    public int replyCount = 0;
    //public List<String> attachments = null;
    public List<AttachmentJson> attachments;
    public List<LocalMessage> replies;

    private LocalMessage() {
        attachments = new ArrayList<AttachmentJson>();
        replies = new LinkedList<LocalMessage>();
    }

    public static LocalMessage copyFromMessage(Message m, boolean myMessage) {
        LocalMessage rr = new LocalMessage();
        if(null == m) return rr;

        rr.id = m.id;
        if (m.inReplyTo != null){
            rr.replyTo = m.inReplyTo.id;
        }else{
            rr.replyTo = -1;
        }
        rr.title = m.title;
        rr.messageContent = m.messageContent;
        rr.postTime = m.postTime;
        rr.voteScore = m.voteScore;
        rr.myMessage = myMessage;
        rr.replyCount = m.replyCount;
        return rr;
    }

    public static LocalMessage copyFromMessageAndVote(Message m, boolean myMessage, Vote vote) {
        LocalMessage rr = copyFromMessage(m, myMessage);
        if(null == vote) return rr;
        if(vote.message.equals(m)) {
            rr.myVote = vote.isUp ? 1 : -1;
        }
        return rr;
    }

//    public static LocalMessage copyFromMessage(final Message m, final User u) {
//        LocalMessage rr = copyFromMessage(m, false);
//        if(null != u) {
//            if(u.equals(m.author)) {
//                rr.myMessage = true;
//            }
//            for (Vote v: m.votes){
//                int score = v.isUp ? 1 : -1;
//                if (u.equals(v.user)){
//                    rr.myVote = score;
//                    //break;
//                }
//                rr.replyCount += score;
//            }
//        }
//        return rr;
//    }

    public static LocalMessage copyFromMessage(Message m, User u){
        LocalMessage rr = copyFromMessage(m, false);
        if(null != u) {
            if(u.equals(m.author)) {
                rr.myMessage = true;
            }
            for (Vote v: m.votes){
                int score = v.isUp ? 1 : -1;
                if (u.equals(v.user)){
                    rr.myVote = score;
                    break;
                }
            }
        }

        if (m.attachment != null){
            if (rr.attachments == null){
                rr.attachments = new ArrayList<AttachmentJson>(m.attachment.size());
            }
            for (Attachment a : m.attachment){
                /**
                 * Server side should not make assumptions about environment config.
                 * */
                rr.attachments.add(a.toJsonClass());
            }
        }

        return rr;
    }
    /**
     * TODO: Multi-media message
     * */
}
