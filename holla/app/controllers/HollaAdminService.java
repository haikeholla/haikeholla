package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.request.RegisterUserIn;
import controllers.response.RegisterUserOut;
import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by jigsaw on 2015/10/29.
 */
@Named
@Singleton
public class HollaAdminService extends Controller {
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final TaggingRepository taggingRepository;
    private final AttachmentRepository attachmentRepository;

    @Inject
    public HollaAdminService(final UserRepository userRepository,
                              final MessageRepository messageRepository,
                              final TaggingRepository taggingRepository,
                              final AttachmentRepository attachmentRepository ) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.taggingRepository = taggingRepository;
        this.attachmentRepository = attachmentRepository;
    }
    /**
     * REST Service: /registerUser
     *
     Input: RegisterUserIn.  Sample Input

     {
     "phoneNum":"919-371-8188",
     "handleName":"BigCheese",
     "location":{
     "longitude":"34.137636",
     "latitude":"84.134089"
     }
     }

     Output: RegisterUserOut.  Sample Output
     {
     "id":1,
     "authToken":"2663e530-0fc6-4a6a-acac-7b69afe04012",
     "createDate":1433692665792,
     "tokenDate":1433692665792
     }
     *
     * */

    public Result registerEditorSvc() {

        JsonNode json = request().body().asJson();
        Logger.debug("Register User with: " + json);

        RegisterUserIn input = null;

        input =  Json.fromJson(json, RegisterUserIn.class);

        User newUser = User.registerUser(input.phoneNum, input.handleName);
        userRepository.save(newUser);

        RegisterUserOut output = new RegisterUserOut(newUser);

        JsonNode jsonout = Json.toJson(output);
        Logger.debug("SUCCESS: Register User : " + jsonout);
        // TODO: send activation email to user
        return ok(jsonout);
    }

    public Result mainPage() {
        return ok(views.html.admin_main.render());
    }

    public Result activateUserSvc() {
        JsonNode json = request().body().asJson();
        Logger.debug("Activate User with: " + json);
        Logger.debug(json.get("authToken").asText());
        User user = this.userRepository.findByAuthTokenAndHandleName(json.get("authToken").asText(), json.get("handle").asText());
        if(null != user) {
            Logger.debug(user.authToken);
            String role = json.get("userRole").asText().trim();
            user.role = User.Role.fromValue(role);
            this.userRepository.save(user);
            //Http.Context.current().args.put(Setting.SESSION_USER, user);
            //return ok(views.html.editor_main.render());
            RegisterUserOut output = new RegisterUserOut(user);
            return ok(Json.toJson(output));
        }
        return badRequest();
    }
}
//77f8ec9b-e19a-4618-a8dd-eb5eea89f786
//xiaoliu