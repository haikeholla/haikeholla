package controllers.response;

import models.User;

import java.util.Date;

/**
 * Created by jigsaw on 2015/10/28.
 */
public class RegisterUserOut {
    /**
     * Sending ID is OK, since this is the owner of that user.
     * */
    public long id;
    public String authToken;
    public Date createDate;
    public Date tokenDate;

    public RegisterUserOut(User u){
        id = u.id;
        authToken = u.authToken;
        createDate = u.createDate;
        tokenDate = u.tokenDate;
    }
}
