package controllers.response;

/**
 * Created by jigsaw on 2015/10/19.
 */
public class ImageAttachmentJson extends AttachmentJson {
    public int imageW;
    public int imageH;
    public int thumbnailW;
    public int thumbnailH;
    public ImageAttachmentJson() {
        super();
    }
    public ImageAttachmentJson(
            String type,
            String path,
            int imageW,
            int imageH,
            int thumbnailW,
            int thumbnailH) {
        super(type, path);
        this.imageW = imageW;
        this.imageH = imageH;
        this.thumbnailW = thumbnailW;
        this.thumbnailH = thumbnailH;
    }
}
