package controllers.response;

/**
 * Created by jigsaw on 2015/10/25.
 */
public class SimpleResponseJson {
    public StatusCode status;

    public SimpleResponseJson(StatusCode status){
        this.status = status;
    }
}
