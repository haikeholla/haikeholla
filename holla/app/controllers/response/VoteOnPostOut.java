package controllers.response;

/**
 * Created by jigsaw on 2015/11/4.
 */
public class VoteOnPostOut {
    // this is the message id
    public long id;
    public int  voteScore;
    public int  myVote;
    public boolean messageDeleted;
}
