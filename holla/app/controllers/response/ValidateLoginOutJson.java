package controllers.response;

/**
 * Created by jigsaw on 2015/11/2.
 */
public class ValidateLoginOutJson extends SimpleResponseJson {
    public String nextPage;
    public ValidateLoginOutJson(StatusCode statusCode, String nextPage){
        super(statusCode);
        this.nextPage = nextPage;
    }
}
