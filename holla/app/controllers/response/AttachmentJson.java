package controllers.response;

/**
 * Created by jigsaw on 2015/10/19.
 */
public class AttachmentJson {
    public String type;
    public String path;

    public AttachmentJson(){}

    public AttachmentJson(String type,
                      String path) {
        this.type = type;
        this.path = path;
    }
}
