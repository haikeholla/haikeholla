package controllers.response;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jigsaw on 2015/10/25.
 */
public class ReplyJson {

    private class Reply {
        public String reply;
        public Date postTime;
        public int voteScore;
        public Reply(String r, Date d, int v) {
            reply = r;
            postTime = d;
            voteScore = v;
        }
    }

    public String originalPost;
    public List<Reply> replies;
    public ReplyJson(String op) {
        this.originalPost = op;
        replies = new LinkedList<Reply>();
    }

    public void addReply(String m, Date d, int v) {
        replies.add(new Reply(m, d, v));
    }
}
