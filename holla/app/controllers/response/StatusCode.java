package controllers.response;

/**
 * Created by jigsaw on 2015/10/9.
 */
public enum StatusCode {
    REQUEST_SUCCESS,
    INVALID_HEADER_X_AUTH_TOKEN,
    INVALID_REQUEST_USER,
    INVALID_REQUEST_BODY_JSON,
    NO_SUCH_MESSAGE,
    NO_IMAGE_UPLOADED,
    FAILED_TO_SAVE_IMAGE,
    MESSAGE_ALREADY_DELETED;
}
