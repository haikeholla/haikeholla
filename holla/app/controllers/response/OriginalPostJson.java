package controllers.response;

/**
 * Created by jigsaw on 2015/10/25.
 */
public class OriginalPostJson {
    public int voteScore;
    public int replyCount;

    public OriginalPostJson(int v, int r) {
        this.voteScore = v;
        this.replyCount = r;
    }
}
