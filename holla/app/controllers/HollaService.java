package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import controllers.request.LocalMessage;
import controllers.request.PostMessageIn;
import controllers.request.RegisterUserIn;
import controllers.request.VoteOnPostIn;
import controllers.response.*;
import filters.NaiveTargeting;
import models.*;
import org.apache.commons.io.FileUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import setting.Setting;
import util.ImageUtil;
import util.JsonUtil;
import util.SimpleImageInfo;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * The main set of web services.
 */
@Named
@Singleton
public class HollaService extends Controller {

	private final UserRepository userRepository;
	private final MessageRepository messageRepository;
	private final VoteRepository voteRepository;
	private final TaggingRepository taggingRepository;
	private final AttachmentRepository attachmentRepository;

	private final static SimpleResponseJson INVALID_USER_REP
			= new SimpleResponseJson(StatusCode.INVALID_REQUEST_USER);

	private final static String INVALID_USER_REP_JSON = Json.toJson(INVALID_USER_REP).toString();
//	private DataSource ds;
//	private Connection conn = null;

	// We are using constructor injection to receive a repository to support our
	// desire for immutability.
	@Inject
	public HollaService(final UserRepository userRepository, 
						final MessageRepository messageRepository, 
						final VoteRepository voteRepository, 
						final TaggingRepository taggingRepository, 
						final AttachmentRepository attachmentRepository ) {
		this.userRepository = userRepository;
		this.messageRepository = messageRepository;
		this.voteRepository = voteRepository;
		this.taggingRepository = taggingRepository;
		this.attachmentRepository = attachmentRepository;
//		this.ds = DB.getDataSource();
//		try {
//			conn = ds.getConnection();
//		} catch(SQLException sqle) {
//			Logger.error("Application: can not connect to DB!");
//		}
	}

	/**
	 * REST Service: /registerUser
	 * 
		 Input: RegisterUserIn.  Sample Input 
	
				{
					"phoneNum":"919-371-8188",
					"handleName":"BigCheese",
					"location":{
						"longitude":"34.137636",
						"latitude":"84.134089"
					}
				}
	
		 Output: RegisterUserOut.  Sample Output
		 {	
		 	"id":1,
		 	"authToken":"2663e530-0fc6-4a6a-acac-7b69afe04012",
		 	"createDate":1433692665792,
		 	"tokenDate":1433692665792
		 }
	 *  
	 * */
	
	public Result registerUserSvc() {
		
		JsonNode json = request().body().asJson();
		Logger.debug("Register User with: " + json);

		RegisterUserIn input = null;
		
		input =  Json.fromJson(json, RegisterUserIn.class);

		User newUser = User.registerUser(input.phoneNum, input.handleName, input.location);
		userRepository.save(newUser);

		RegisterUserOut output = new RegisterUserOut(newUser);

		JsonNode jsonout = Json.toJson(output);
		Logger.debug("SUCCESS: Register User : " + jsonout);
		
        return ok(jsonout);
	}
	
	/**
	 * TODO: Design security model
	 * 
	 * ALthough we do not ask the user to authenticate, we still need to have a transparent 
	 * authentication model for two reasons: 
	 * 
	 * 1. For user requests:  We need to authenticate the request, to make sure it comes from a valid Holla app
	 * 2. For business account request: Obviously this needs to be authenticated.  
	 * */
	
	/**
	 * Private method for finding the current user. 
	 * */
	
    private User getUser() {
    	
    	/**
    	 * First, check if there is a user in the current session.  This depends on the cookie.
    	 * This is meant for the web app  
    	 * */
        User user = (User)Context.current().args.get(Setting.SESSION_USER);
        
    	/**
    	 * If there is no user in the session, get the Auth Token Header.  This is meant for the API client.   
    	 * */
        if (user == null) {
    		Logger.debug("No User found in current context.");

			String[] authTokenHeaderValues = Context.current().request().headers().get(Setting.AUTH_TOKEN_HEADER);
			if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {

				Logger.debug("Autho Header Value: " + authTokenHeaderValues[0]);

				user = userRepository.findByAuthToken(authTokenHeaderValues[0]);
				if (user != null) {
					Context.current().args.put(Setting.SESSION_USER, user);
					Logger.debug("Found user from header: " + user.id);
					return user;
				}
			}
			String authToken = session(Setting.SESSION_USER_AUTH_TOKEN);
			if(authToken == null) {
				return user;
			}
			user = userRepository.findByAuthToken(authToken);
			if (user != null) {
				Context.current().args.put(Setting.SESSION_USER, user);
				Logger.debug("Found user from current session: " + user.id);
				return user;
			}
        } else {
    		Logger.debug("Found user in current context: " + user.id);
        }

        return user;
    }
    
	/**
	 * REST Service: /getLocalMessages
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
	 	
	 	Input: Location.  Sample
	 	{
	 		location:{
				"longitude":"34.137636",
				"latitude":"84.134089"
			}, 
			type:new,
			page:1,
	 		voteDesc:true
	 	}

	 	Output: List<LocalMessage> , Sample
	 	[
	 		{
	 			"id":1,
	 			"messageContent":"小孩为什么永远都不听话？  #beingparents",
	 			"postTime":1433695465160,
	 			"voteScore":0,
	 			"myMessage":true,
	 			"myVote":0
	 		}
	 	]
	 * */
	
	public Result getLocalMessagesSvc() {
		User u = this.getUser();
		if(null == u) {
			return badRequest("invalid user!");
		}
		JsonNode json = request().body().asJson();

		Logger.debug("Get Local Messages for: " + json);

		Location location =  Json.fromJson(json.get("location"), Location.class);
		int page = JsonUtil.get(json, "page", 0, Integer.class);
		boolean voteDesc = JsonUtil.get(json, "voteDesc", false, Boolean.class);

        final Iterable<Message> allMessages = this.getLocalMessages(location, page, voteDesc);
        List<LocalMessage> allRet = new ArrayList<LocalMessage>();
        
        for (Message mes : allMessages){
        	
        	LocalMessage lm = LocalMessage.copyFromMessage(mes, u);
        	
        	allRet.add(lm);
        }
        
        return ok(Json.toJson(allRet));
	}
	
	private Iterable<Message> getLocalMessages(Location loc, int page, boolean voteDesc) {
		
		String geoTag = Tagging.TaggingParser.getGeographyTag(loc);

		Logger.debug("Mapped location [" + loc.longitude + "," + loc.latitude + "] to geoTag [" + geoTag + "]");

        return this.getPostsForTag(geoTag, page, voteDesc);
	}
	
	
	/**
	 * TODO: Plugin a more robust Geo Tagging strategy
	 * */
	private String getGeographyTag(Location loc){
		return "/Space/Earth";
	}

	/**
	 * REST Service: /getHomeMessages
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
	 	Output: List<LocalMessage> , Sample
	 	[
	 		{
	 			"id":1,
	 			"messageContent":"小孩为什么永远都不听话？  #beingparents",
	 			"postTime":1433695465160,
	 			"voteScore":0,
	 			"myMessage":true,
	 			"myVote":0
	 		}
	 	]
	 * */
	public Result getHomeMessagesSvc(int page) {
		
		User u = getUser();
		
		if (u != null && u.homeLocation != null){
			Iterable<Message> allMessages = getLocalMessages(u.homeLocation, page, false);
			return ok(Json.toJson(allMessages));
		}
		
		return null;
	}

	public Result getPostsForTagSvc(String tag, int page) {

        final Iterable<Message> allMessages = this.getPostsForTag(tag, page, false);
        return ok(Json.toJson(allMessages));
	}

	private Iterable<Message> getPostsForTag(String tag, int page, boolean voteDesc) {
		
		Logger.debug("Get Posts for Tag: " + tag);

        Iterable<Tagging> allTags = null;
		if(voteDesc) {
			allTags = this.taggingRepository.findByTagAndMessageAuthorRoleOrderByMessageVoteScoreDescMessagePostTimeDesc(tag, User.Role.REGULAR, new PageRequest(page, Setting.MESSAGE_PER_PAGE));
		} else {
			allTags = this.taggingRepository.findByTagAndMessageAuthorRoleOrderByMessagePostTimeDesc(tag, User.Role.REGULAR, new PageRequest(page, Setting.MESSAGE_PER_PAGE));
		}
        final List<Message> allMessages = new ArrayList<Message>();
        
        for (Tagging t: allTags){
				allMessages.add(t.message);
        }
        
        return allMessages;
	}

	private List<Message> getRecommendedTopicsByTag(String tag, int page, boolean voteDesc) {
		Logger.debug("Get Recommended Topics for Tag: " + tag);
		List<Tagging> allTags = null;
		if(voteDesc) {
			allTags = this.taggingRepository.findByTagAndMessageAuthorRoleOrderByMessageVoteScoreDescMessagePostTimeDesc(tag, User.Role.EDITOR, new PageRequest(page, Setting.MESSAGE_PER_PAGE));
		} else {
			allTags = this.taggingRepository.findByTagAndMessageAuthorRoleOrderByMessagePostTimeDesc(tag, User.Role.EDITOR, new PageRequest(page, Setting.MESSAGE_PER_PAGE));
		}
		final List<Message> allMessages = new ArrayList<Message>(allTags.size());
		for(Tagging t : allTags) {
			allMessages.add(t.message);
		}
		return allMessages;
	}
	
	/**
	 * REST Service: /postMessage
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
		Input: PostOrigMessageIn.  Sample Input 
	
		{
			"message":"小孩为什么永远都不听话？  #beingparents",
			"location":{
				"longitude":"34.137636",
				"latitude":"84.134089"
			}
		}
	
	 	Output: LocalMessage , Sample
	 	[
	 		{
	 			"id":1,
	 			"messageContent":"小孩为什么永远都不听话？  #beingparents",
	 			"postTime":1433695465160,
	 			"voteScore":0,
	 			"myMessage":true,
	 			"myVote":0
	 		}
	 	]
	 * */
	public Result postMessageSvc() {
		
		JsonNode json = request().body().asJson();
		Logger.debug("Post message with JSON [" + json + "]" );

		PostMessageIn postMes =  Json.fromJson(json, PostMessageIn.class);
		if(null == postMes || Strings.isNullOrEmpty(postMes.message) || null == postMes.location) return badRequest();
		User u = this.getUser();
		if(null == u) return badRequest(INVALID_USER_REP_JSON);

		/**
		 * 1. Update User Location
		 * */
		u.location = postMes.location;
		this.userRepository.save(u);
		Logger.debug("Updated location for user[" + u + "] at location [" + postMes.location + "]" );

		// generate message
		Message m = new Message(u, postMes);
		/**
		 * Process tags
		 * */
		Tagging.TaggingParser.fillTag(m);
		this.messageRepository.save(m);
		this.taggingRepository.save(m.tagging);
		LocalMessage retMes = LocalMessage.copyFromMessage(m, u);

		return null == retMes ? badRequest(INVALID_USER_REP_JSON) : ok(Json.toJson(retMes));
//
//		if (postMes != null && u != null && postMes.message != null && postMes.location != null){
//
//			Message m = postMessage(postMes.title + Setting.TITLE_MESSAGE_DELIMITER + postMes.message, u, postMes.location);
//
//			LocalMessage retMes = new LocalMessage();
//
//			retMes.id = m.id;
//			retMes.messageContent = m.messageContent;
//			retMes.myMessage = true;
//			retMes.myVote = 0;
//			retMes.postTime = m.postTime;
//			retMes.voteScore = 0;
//
//	        return ok(Json.toJson(retMes));
//		}
//		return badRequest(INVALID_USER_REP_JSON);

	}

	/**
	 * REST Service: /postPicture
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
		Input: DataForm.  Sample Input 
	
		{
			"messageID":62,
			"picture":file data
		}
	 * */
	public Result postPictureSvc(){
		MultipartFormData body = request().body().asMultipartFormData();
		PostMobilePictureOut ret = new PostMobilePictureOut(-1);
		String[] headers = Controller.request().headers().get(Setting.AUTH_TOKEN_HEADER);
		if(headers == null || headers.length == 0) {
			ret.status = StatusCode.INVALID_HEADER_X_AUTH_TOKEN.ordinal();
			return ok(Json.toJson(ret));
		}
		FilePart picture = body.getFile("picture");
		String[] messageIDs = body.asFormUrlEncoded().get("messageID");
		if(messageIDs == null) {
			ret.status = StatusCode.NO_SUCH_MESSAGE.ordinal();
			//return badRequest("Failed to upload picture!");
			return ok(Json.toJson(ret));
		}
		for (String mid : messageIDs){
			Logger.debug("Uploaded Picture for message id: " + mid);
		}
		Long mid = Long.valueOf(messageIDs[0]);
		/*
		 * this check may not seem to be fine: need to update
		 * we need more check to validate the request is orignated
		 * from "this" user and is for "this" message
		 */
		Message m = this.messageRepository.findOne(mid);

		if (picture != null && m != null) {
			Logger.debug("FindOne : " + m.messageContent);
			ret.messageID = m.id;
			String fileName = picture.getFilename();
			String contentType = picture.getContentType();
			File file = picture.getFile();
			try {
				File storedFile = new File("public/images/"+ messageIDs[0], fileName);
				FileUtils.moveFile(file, storedFile);
				String thumbnailStoredPath = "public/images/"+ messageIDs[0] + "/" + Setting.THUMBNAIL_PREFIX + fileName;
				String displayURLPrefix = Setting.ASSETS_PREFIX + "/" + messageIDs[0];
				SimpleImageInfo d = ImageUtil.produceThumbnailWithFixedWidth(
						storedFile,
						Setting.THUMBNAIL_WIDTH,
						thumbnailStoredPath);
				SimpleImageInfo thumbnailInfo = ImageUtil.getThumbnailImageInfo(d);
//				if(d == null) {
//					Logger.error("can not produce thumbnail for: " + fileName);
//				} else {
//					Logger.debug("thumbnail dimension is " + d);
//					ret.thumbnails.add(new ImageUtil.ImageInfo(displayURLPrefix + "/" + Setting.THUMBNAIL_PREFIX + fileName, d));
//				}
				Logger.debug("thumbnail dimension is " + thumbnailInfo);
				ret.thumbnails.add(new ImageUtil.ImageInfo(displayURLPrefix + "/" + Setting.THUMBNAIL_PREFIX + fileName, thumbnailInfo));
				Attachment a = new Attachment();
				a.message = m;
				a.path = mid + "/" + fileName;
				a.type = Attachment.Type.image.name();
				//a.setType(Attachment.Type.image);
				a.thumbnailWidth = thumbnailInfo.getWidth();
				a.thumbnailHeight = thumbnailInfo.getHeight();
				a.imageHeight = d.getHeight();
				a.imageWidth = d.getWidth();

				if (m.attachment == null){
					m.attachment = new HashSet<Attachment>();
				}
				m.attachment.add(a);

				this.attachmentRepository.save(a);
				this.messageRepository.save(m);
				Logger.debug("Uploaded Picture: " + fileName);
				ret.status = StatusCode.REQUEST_SUCCESS.ordinal();
	        } catch (IOException ioe) {
	        	Logger.error("Failed to move Picture: " + fileName + " for message: " + messageIDs[0]);
				ret.status = StatusCode.FAILED_TO_SAVE_IMAGE.ordinal();
				ret.failedPictures.add(fileName);
	        }
		} else {
			//flash("error", "Missing file");
			//return redirect(routes.Application.index());
			if(picture == null) {
				ret.status = StatusCode.NO_IMAGE_UPLOADED.ordinal();
			}
			if(m == null) {
				ret.status = StatusCode.NO_SUCH_MESSAGE.ordinal();
			}
		}
		//Logger.debug(Json.toJson(ret).toString());
		return ok(Json.toJson(ret));
	}

	private Message postMessage(String message, User u, Location loc) {
		
		if (message != null && u != null){
			
			Logger.debug("Post message [" + message + "] for user[" + u + "] at location [" + loc + "]" );
			
			/**
			 * 1. Update User Location
			 * */
			u.location = loc;
			this.userRepository.save(u);
			Logger.debug("Updated location for user[" + u + "] at location [" + loc + "]" );
			
			Message m = new Message(u, message);
			m.location = loc;
			m.tagging = new ArrayList<Tagging>();
			
			/**
			 * Process tags 
			 * */
			
			Pattern MY_PATTERN = Pattern.compile("#(\\w+|\\W+)");
			Matcher mat = MY_PATTERN.matcher(message);
			while (mat.find()) {
				String tag = mat.group(1);
				Tagging t = new Tagging(tag, m);
				m.tagging.add(t);
				Logger.debug("Add Tag: " + tag);
			}			
			/**
			 * Add location tag 
			 * */
			String geoTag = this.getGeographyTag(loc);
			Tagging t = new Tagging(geoTag, m);
			m.tagging.add(t);
			Logger.debug("Add geo Tag: " + geoTag);
			
			this.messageRepository.save(m);
			
			this.taggingRepository.save(m.tagging);
	        
			return m;
		}
		return null;
	}

	/**
	 * REST Service: /postReply
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
		Input: PostMessageIn.  Sample Input 
	
		{
			"replyTo":84764
			"message":"打！",
			"location":{
				"longitude":"34.137636",
				"latitude":"84.134089"
			}
		}
	
	 	Output: LocalMessage , Sample
	 	[
	 		{
	 			"id":1,
	 			"messageContent":"小孩为什么永远都不听话？  #beingparents",
	 			"postTime":1433695465160,
	 			"voteScore":0,
	 			"myMessage":true,
	 			"myVote":0
	 		}
	 	]
	 **/
	@Transactional
	public Result postReplySvc() {
		
		JsonNode json = request().body().asJson();
		Logger.debug("Post reply with JSON [" + json + "]" );

		PostMessageIn postRep =  Json.fromJson(json, PostMessageIn.class);

		User u = this.getUser();
		
		if (postRep != null && u != null && postRep.message != null){
			Message op = this.messageRepository.findByIdAndFetchRepliesEagerly(postRep.replyTo);
			
			if (op != null){
				Message m = new Message(u, postRep.message, op, postRep.location);
				op.replies.add(m);
				op.replyCount = op.replies.size();

				this.messageRepository.save(m);
				this.messageRepository.save(op);
				LocalMessage repMes = LocalMessage.copyFromMessage(op, u.equals(op.author));

				return ok(Json.toJson(repMes));
			}
		}

		return badRequest("Cannot find message or user");

	}

	/**
	 * REST Service: /getReplies
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
		Input: opid
	
	 	Output: LocalMessage , Sample
	 	[
	 		{
	 			"id":1,
	 			"messageContent":"小孩为什么永远都不听话？  #beingparents",
	 			"postTime":1433695465160,
	 			"voteScore":0,
	 			"myMessage":true,
	 			"myVote":0,
	 			"replies": [
	 				{}, 
	 				{}, 
	 				{}
	 			]
	 		}
	 	]
	 **/
	
	public Result getRepliesSvc(long opid, Integer page) {
        //final Message op = this.messageRepository.findByIdAndFetchRepliesEagerly(opid);
        final Message op = this.messageRepository.findById(opid);
		Logger.debug("get reply page " + page);
		final List<Message> replies =
				this.messageRepository.findByInReplyToOrderByPostTimeDesc(op, new PageRequest(page, Setting.REPLY_PER_PAGE));
        Logger.debug("there are " + replies.size() + " replies!");
        if (op.replyCount != replies.size()){
        	op.replyCount = replies.size();
        	this.messageRepository.save(op);
        }
        
        User u = this.getUser();

        LocalMessage opRet = LocalMessage.copyFromMessage(op, u);

        for (Message r : replies){
			List<Vote> myVotes = this.voteRepository.findByUserAndMessage(u, r);
			Vote myVote = myVotes.size() > 0 ? myVotes.get(0) : null;
        	LocalMessage rr = LocalMessage.copyFromMessageAndVote(op, u.equals(op.author), myVote);
//        	rr.id = r.id;
//        	rr.replyTo = opid;
//        	rr.messageContent = r.messageContent;
//        	rr.postTime = r.postTime;
//        	rr.voteScore = r.voteScore;
//
//        	if (u != null){
//        		if (u.equals(r.author)){
//        			rr.myMessage = true;
//        		}
//
//        		for (Vote v: r.votes){
//        			if (u.equals(v.user)){
//        				rr.myVote = v.isUp ? 1 : -1;
//        				break;
//        			}
//        		}
//        	}
//
        	opRet.replies.add(rr);
        }
        return ok(Json.toJson(opRet));
	}

	//	public Result getMyOrigPosts() {
//
//		User u = this.getUser();
//
//		if (u != null){
//	        final Iterable<Message> allMessages = this.messageRepository.findByAuthor(getUser());
//	        final List<Message> retMessages = new ArrayList<Message>();
//
//	        for (Message m : allMessages){
//	        	if (m.inReplyTo == null) retMessages.add(m);
//	        }
//
//	        return ok(Json.toJson(retMessages));
//		}
//
//		return null;
//	}

	/*
    /getMyRepliesSvc
    Required Header: X-AUTH-TOKEN

    Input: JSON Object.  Sample Input

    {
        "messageID":62
    }
    */
	public Result getMyRepliesSvc() {
		
		User u = this.getUser();
		
		if (u != null){
//	        final Iterable<Message> allMessages = this.messageRepository.findByAuthor(u);
//	        final List<Message> retMessages = new ArrayList<Message>();
//
//	        for (Message m : allMessages){
//	        	if (m.inReplyTo != null) retMessages.add(m);
//	        }
			Map<Long, String> mapping = new HashMap<Long, String>();
			Map<Long, ReplyJson> ret = new HashMap<Long, ReplyJson>();
	        List<Message> retMessages = this.messageRepository.findByAuthorAndInReplyToNotNullOrderByPostTimeDesc(u);
			for(Message m : retMessages) {
				if(!ret.containsKey(m.inReplyTo.id)) {
					Message post = this.messageRepository.findById(m.inReplyTo.id);
					ReplyJson replyJson = new ReplyJson(post==null?"原帖已删除":post.messageContent);
					replyJson.addReply(m.messageContent, m.postTime, m.voteScore);
					ret.put(m.inReplyTo.id, replyJson);
				} else {
					ReplyJson replyJson = ret.get(m.inReplyTo.id);
					replyJson.addReply(m.messageContent, m.postTime, m.voteScore);
				}
			}
			return ok(Json.toJson(ret.values()));
		}
		
		return badRequest(Json.toJson(new SimpleResponseJson(StatusCode.INVALID_REQUEST_USER)));
	}

	public Result getMyVoted() {
		
		User u = this.getUser();
		
		if (u != null){
	        final Iterable<Vote> allVotes = this.voteRepository.findByUser(u);
	        return ok(Json.toJson(allVotes));
		}
		
		return null;
	}

	/**
	 * REST Service: /voteOnPost
	 *
	 	Required Header: X-AUTH-TOKEN
	 	
		Input: opid
	 		{
	 			"id":1,
	 			"upVote":true
	 		}
	 	Output: LocalMessage , Sample
	 		{
	 			"id":1,
	 			"voteScore":1,
	 			"myVote":0
	 		}
	 **/

	public Result voteOnPostSvc() {
		
		JsonNode json = request().body().asJson();
		Logger.debug("Vote with JSON [" + json + "]" );

		VoteOnPostIn voteIn =  Json.fromJson(json, VoteOnPostIn.class);

		User u = this.getUser();
		
		Message message = this.messageRepository.findOne(voteIn.id);
		
		if (message != null && u != null){
			
			List<Vote> votes = this.voteRepository.findByUserAndMessage(u, message);

			VoteOnPostOut voteOut =  new VoteOnPostOut();
			voteOut.id = voteIn.id;

			if (votes != null && votes.size() > 0){
				if (voteIn.upVote ^ votes.get(0).isUp) {
					// votes in a different direction, delete vote.
					message.voteScore += (voteIn.upVote? 1 : -1);
					if(message.voteScore <= Setting.DELETE_MSG_VOTESCORE_THRESHOLD) {
						// do we need to really delete this message from DB
						// or use a flag to indicate it was deleted?
						this.messageRepository.delete(message);
						voteOut.messageDeleted = true;
					} else {
						message.votes.remove(votes.get(0));
						this.messageRepository.save(message);

						this.voteRepository.delete(votes.get(0));
						voteOut.myVote = 0;
					}
				} else {
					// Votes in the same direction.  Do nothing. 
					// A user can only vote once.
					voteOut.myVote = (voteIn.upVote ? 1 : -1);
				}
			} else {
				Vote v = new Vote(message, u, voteIn.upVote);
				message.votes.add(v);
				this.voteRepository.save(v);
				
				message.voteScore += (voteIn.upVote? 1 : -1);
				this.messageRepository.save(message);
				voteOut.myVote = (voteIn.upVote? 1 : -1);
			}
			
			voteOut.voteScore = message.voteScore;
			return ok(Json.toJson(voteOut));
		}
		return badRequest(Json.toJson(new SimpleResponseJson(StatusCode.INVALID_REQUEST_USER)));
	}

	public Result getHotTags() {
		
		/**
		 * TODO:  Change Logic to find hot tags
		 * 
		 * Need to override JPQL query
		 * */

        final Iterable<Message> allMessages = this.messageRepository.findAll();
        return ok(Json.toJson(allMessages));
	}

	/*
		/postMobilePictureSvc
		Required Header: X-AUTH-TOKEN

		Input: JSON Object.  Sample Input

		{
			"messageID":62,
			"pictures":[base64_encoded_image_string1, base64_encoded_image_string2, ...]
		}
	 */
	static class EncodedImage {
		public String fileName;
		public String encodedStr;
	}
	static class PostMobilePictureIn {
		public long messageID;
		public List<EncodedImage> pictures = new LinkedList<EncodedImage>();
	}
	static class PostMobilePictureOut {
		public long messageID;
		public List<String> failedPictures;
		public List<ImageUtil.ImageInfo> thumbnails;
		public PostMobilePictureOut(long messageID) {
			this.messageID = messageID;
			this.failedPictures = new LinkedList<String>();
			this.thumbnails = new LinkedList<ImageUtil.ImageInfo>();
		}
		public int status;
	}

	/*
	 * leave it for now, in the future we need to compare if the user who sends
	 * the pictures are the user in the current session
	 */
	private User getRequestUser(final String auth) {
		User user = this.userRepository.findByAuthToken(auth);
		return user;
	}
    //@BodyParser.Of(BodyParser.Json.class)
	@Transactional
	public Result postMobilePictureSvc(){
		JsonNode json = Controller.request().body().asJson();
		Logger.debug("application: postMobilePictures recv " + json);
		PostMobilePictureOut ret = new PostMobilePictureOut(-1);
		if(json == null) {
			return badRequest(Json.toJson(ret));
		}
		String[] headers = Controller.request().headers().get(Setting.AUTH_TOKEN_HEADER);
		if(headers == null || headers.length == 0) {
			ret.status = StatusCode.INVALID_HEADER_X_AUTH_TOKEN.ordinal();
			return badRequest(Json.toJson(ret));
		}
		String auth = Controller.request().headers().get(Setting.AUTH_TOKEN_HEADER)[0];
		User requestUser = getRequestUser(auth);
		User sessionUser = getUser();
		if(requestUser == null || sessionUser == null || requestUser.id != sessionUser.id) {
			ret.status = StatusCode.INVALID_REQUEST_USER.ordinal();
			return badRequest(Json.toJson(ret));
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			PostMobilePictureIn mobilePictureIn = mapper.treeToValue(json, PostMobilePictureIn.class);
			Message message = this.messageRepository.findById(Long.valueOf(mobilePictureIn.messageID));
			if (message != null) {
				if (message.attachment == null) {
					message.attachment = new HashSet<Attachment>();
				}
				ret.messageID = mobilePictureIn.messageID;
				for (EncodedImage image : mobilePictureIn.pictures) {
					// save file first
						String storedDir = Setting.ATTACHMENT_IMAGE_STORE_PREFIX + "/" + mobilePictureIn.messageID;
						String imageStoredPath = storedDir + "/" + image.fileName;
					    String thumbnailStoredPath = storedDir  + "/" + Setting.THUMBNAIL_PREFIX + image.fileName;
					    String displayURLPrefix = Setting.ASSETS_PREFIX + "/" + mobilePictureIn.messageID;
					if(!Files.isDirectory(Paths.get(storedDir))) {
						File dir = new File(storedDir);
						dir.mkdirs();
					}
						if(ImageUtil.saveBase64ImageStrAsFile(image.encodedStr, imageStoredPath)) {
							// make thumbnails
							if(ImageUtil.produceThumbnail(
									imageStoredPath,
									Setting.THUMBNAIL_HEIGHT,
									Setting.THUMBNAIL_WIDTH,
									Setting.THUMBNAIL_FACTOR,
									thumbnailStoredPath)) {
								//ret.thumbnails.add(new ImageUtil.ImageInfo(displayURLPrefix+ "/" + Setting.THUMBNAIL_PREFIX + image.fileName);
							}
							Attachment attachment = new Attachment();
							attachment.message = message;
							attachment.path = message.id + "/" + image.fileName;
							attachment.type = Attachment.Type.image.name();
							//attachment.setType(Attachment.Type.image);
							message.attachment.add(attachment);
							this.attachmentRepository.save(attachment);
						} else {
							ret.failedPictures.add(image.fileName);
						}
						//FileUtils.moveFile(file, new File("public/images/" + mobilePictureIn.messageID, image.fileName));
					/*
					try{
						Base64.decodeToFile(image.encodedStr, imageStoredPath);
						try {
							Thumbnail.generateThumbnail(imageStoredPath, THUMBNAIL_HEIGHT, THUMBNAIL_WIDTH, THUMBNAIL_FACTOR, thumbnailStoredPath);
							ret.thumbnails.add(displayURLPrefix+ "/" + Setting.THUMBNAIL_PREFIX + image.fileName);
						} catch (IOException ioe) {
							ret.thumbnails.add(displayURLPrefix+ "/" + image.fileName);
						}
						Attachment attachment = new Attachment();
						attachment.message = message;
						attachment.path = message.id + "/" + image.fileName;
						attachment.type = Attachment.Type.IMAGE.name().toLowerCase();//Attachment.IMAGE_TYPE;
						message.attachment.add(attachment);
						this.attachmentRepository.save(attachment);
					} catch (IOException ioe) {
						ret.failedPictures.add(image.fileName);
					}
					*/
				}
				this.messageRepository.save(message);
				ret.status = StatusCode.REQUEST_SUCCESS.ordinal();
				return ok(Json.toJson(ret));
			} else {
				ret.status = StatusCode.NO_SUCH_MESSAGE.ordinal();
				return badRequest(Json.toJson(ret));
			}
		} catch(JsonProcessingException jpe) {
			Logger.error("Failed to parse Request: " + json.toString());
			ret.status = StatusCode.INVALID_REQUEST_BODY_JSON.ordinal();
			return badRequest(Json.toJson(ret));
		}
	}
	/*
		/deleteMessageSvc
		Required Header: X-AUTH-TOKEN

		Input: JSON Object.  Sample Input

		{
			"messageID":62
		}
	 */
	public Result deleteMessageSvc(long mid) {
		Logger.debug("found message to delete: " + mid);
		User user = this.getUser();
		if(null != user && null != this.messageRepository.findByIdAndAuthorId(mid, user.id)) {
			this.deleteMessageTagging(mid);
			this.deleteMessageAttachment(mid);
			this.messageRepository.delete(mid);
			return ok(Json.toJson(new SimpleResponseJson(StatusCode.REQUEST_SUCCESS)));
		}
		return badRequest(Json.toJson(new SimpleResponseJson(
				user == null ? StatusCode.INVALID_REQUEST_USER : StatusCode.MESSAGE_ALREADY_DELETED
		)));
	}

	private void deleteMessageTagging(long mid) {
		this.taggingRepository.deleteByMessageId(mid);
	}

	private void deleteMessageAttachment(long mid) {
		Logger.debug("found message to delete: " + mid);
		try {
			FileUtils.deleteDirectory(new File(Setting.ATTACHMENT_IMAGE_STORE_PREFIX + "/" + mid));
		} catch (IOException ioe) {
			Logger.error("there is no attachment for message: " + mid);
		}
		//this.attachmentRepository.deleteByMessage(m);
		this.attachmentRepository.deleteByMessageId(mid);
	}


	/*
		/getMessageSvc
		Required Header: X-AUTH-TOKEN

		Input: JSON Object.  Sample Input

		{
			"messageID":62
		}
	 */
	public Result getMessageSvc(Long mid) {
		Logger.debug("found message : " + mid);
		User user = this.getUser();
		Message m = this.messageRepository.findByIdAndAuthorId(mid, user.id);
		if(null != user && null != m) {
			return ok(Json.toJson(LocalMessage.copyFromMessage(m, user)));
		}

		return badRequest(Json.toJson(new SimpleResponseJson(
				user == null ? StatusCode.INVALID_REQUEST_USER : StatusCode.MESSAGE_ALREADY_DELETED
		)));
	}

	/*
		/getMyPostsSvc
		Required Header: X-AUTH-TOKEN

		Input: JSON Object.  Sample Input (optional)

		{
			"page":2
		}
	 */
	public Result getMyPostsSvc(Integer page) {
		User user = this.getUser();
		int requestPage = page == null ? 0 : page;
		if(null != user) {
			List<Message> messages =
					this.messageRepository.findByAuthorAndInReplyToNullOrderByPostTimeDesc(
							user,
							new PageRequest(page, Setting.MESSAGE_PER_PAGE));
			List<OriginalPostJson> ret = new ArrayList<OriginalPostJson>(messages.size());
			for(Message m : messages) {
				ret.add(new OriginalPostJson(m.voteScore, m.replyCount));
			}
			return ok(Json.toJson(ret));
		}
		return badRequest(INVALID_USER_REP_JSON);
	}

	/*
	     /disover
	     Required Header: X-AUTH-TOKEN
	 */
	public Result disoverSvc(Integer page) {
		User user = this.getUser();
		int requestPage = page == null ? 0 : page;
		if(null != user) {
			String tag = Tagging.TaggingParser.getGeographyTag(user.location);
			List<Message> topics = getRecommendedTopicsByTag(tag, requestPage, true);
			List<Message> targetedTopics = (new NaiveTargeting()).generateTargetedTopicsForUser(user, topics);
			List<LocalMessage> ret = new ArrayList<LocalMessage>(targetedTopics.size());
			for(Message m : targetedTopics) {
				List<Vote> myVotes = this.voteRepository.findByUserAndMessage(user, m);
				Vote myVote = myVotes.size() > 0 ? myVotes.get(0) : null;
				LocalMessage lm = LocalMessage.copyFromMessageAndVote(m, false, myVote);
				ret.add(lm);
			}
			return ok(Json.toJson(ret));
		}
		return badRequest(INVALID_USER_REP_JSON);
	}

	public Result hollaLoginPage() {
		return ok(views.html.main.render());
	}

	public Result validateLoginSvc() {
		JsonNode json = request().body().asJson();
		String username = json.get("username").asText().trim();
		String password = json.get("password").asText().trim();
		Logger.debug(username +":"+ password);
		User user = this.userRepository.findByAuthTokenAndHandleName(password, username);
		if(user == null) {
			return badRequest();
		}
		session(Setting.SESSION_USER_AUTH_TOKEN, user.authToken);
		//Http.Context.current().args.put(Setting.SESSION_USER, user);
		Logger.debug(session(Setting.SESSION_USER_AUTH_TOKEN));
		if(user.role == User.Role.REGULAR) {
			return ok(views.html.index.render("Holla"));
		} else if(user.role == User.Role.EDITOR) {
			return ok(Json.toJson(new ValidateLoginOutJson(StatusCode.REQUEST_SUCCESS, "/editor/main")));
		} else if(user.role == User.Role.ADMIN) {
			return ok(views.html.admin_main.render());
		} else {
			return badRequest();
		}
	}
}
